# Much Ado - A repository of Stata programs

I used to use [Stata](https://www.stata.com/) for all of my statistical analyses^1 and even wrote a package which was accepted to the [Statistical Software Components](https://ideas.repec.org/s/boc/bocode.html) archive ([`genass`](https://ideas.repec.org/c/boc/bocode/s456415.html]), a wrapper for performing case-control analyses in genetic association studies).

Recently I was contacted by a researcher who was looking for the `pwld` programme which was written by [David Clayton](https://en.wikipedia.org/wiki/David_Clayton) and formed part of his `genassoc` package of functions for statistical genetics.  Not being the author of this package I had a quick search and found that his [server](http://www-gene.cimr.cam.ac.uk/clayton/software/stata) is no longer online, nor were his functions submitted to the [SSC](https://ideas.repec.org/s/boc/bocode.html).  I therefore opted to create a [Git](https://git-scm.com/) repository to make the files available (with the added bonus of version control should I ever be inclined to modify anything).

The files are provided as is, with no express warranty, only a few are written by me.  If you are the author of any of the included files and do not wish them to be shared in this manner please contact me and I will remove them.  If you maintain more recent versions please let me know and I will remove them and point to these resources.

## Clone the repository

You should clone the repository to your local computer and then place the files in your [`adopath`](http://www.stata.com/help.cgi?adopath).  To clone the repository...

    git clone git@gitlab.com:nshephard/much-ado.git

Where you put them to be regonised in your `adopath` very much depends on your Operating System and your personal configuration, but I would recommend adding them to your `personal` directory of your `adopath`.  Alternatively you can place the whole `clayton` directory in `~/ado/clayton` and permanently add the path to `adopath` with...

    adopath + "~/ado/clayton"

..modifying the path to reflect your Operating System (the above assumes you are on a UNIX-like operating system and you have the `ado` directory in `$HOME`).


^1 A while back I switched to [R](https://www.r-project.com/) as it seemed to be gaining traction and I figured it would be useful to know more than one statistical software language.
